#
Title = Carbon
CalculationMode = ae + pp
Verbose = 40
#MeshType = 2
#MeshNumberOfPoints = 1000
#MeshOutmostPoint = 90
#MeshStartingPoint = .00001
NuclearCharge = 6
MixingScheme = 1
Mixing = 0.3
#SpinMode = 2
%Orbitals
"He"
 2 | 0 | 1 | 1
 2 | 1 | 2 |0
 3 | 2 | 0 | 0
%

%PPComponents
 2 | 0 | 1.2 | tm
 2 | 1 | 1.2 | tm
 3 | 2 | 1.2 | tm
%

PPOutputFileFormat = siesta
LLocal = 1

#XCFunctional = 101130
XCFunctional = gga_c_am05 + gga_x_am05

## EOF ##
